// swift-tools-version:5.7
import PackageDescription

let package = Package(
    name: "PythonOnMacCore",
    products: [
        .library(name: "PythonOnMacCore", targets: ["PythonOnMacCore"]),
    ],
    targets: [
        .target(name: "PythonOnMacCore",
            resources: [
                .process("../../libpython3.11.dylib")
            ],
            linkerSettings: [
                .unsafeFlags([
                    "-L",
                    ".",
                    "-lpython3.11"
                ])
            ]
       ),
        .testTarget(name: "PythonOnMacCoreTests", dependencies: ["PythonOnMacCore"])
    ]
)
