//
//  You should never be required to directly interface with any methods or code in this file or impl.c
// all required methods and documentation are defined in the PythonOnMac package

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../pythonFiles/Python.h"
