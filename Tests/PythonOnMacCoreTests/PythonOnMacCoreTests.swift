//
//  PythonOnMacCoreTests 2.swift
//  
//
//  Created by Nikhil Sha on 1/2/23.
//

import XCTest
import PythonOnMacCore
import Foundation

/// shouldnt be needed cuz methods r defined in PythonOnMac
public class PythonOnMacCoreTests: XCTestCase {
    public func testHi() {
        Py_Initialize()
        PyRun_SimpleString(("print('pls')" as NSString).utf8String)
    }
}
